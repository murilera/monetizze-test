import random
import re

class Monetizze:

    def __init__ (self):
        self.__qtd_dezenas = 0
        self.__resultado = []
        self.__total = 0
        self.__jogos = []

    def __set_name__(self, name):
        self.name = name

    def __get__(self, obj, type=None) -> object:
        return obj.__dict__.get(self.name)

    def __set__(self, obj, value) -> None:
        obj.__dict__[self.name] = value

    def constroi(self, dezenas, total):
        if dezenas in [6, 7, 8, 9, 10]:
            self.__qtd_dezenas = dezenas
        self.__total = total

    def _gera_dezenas(self):
        array = []
        while len(array) < self.__qtd_dezenas:
            r = random.randint(1, 60)
            if r not in array:
                array.append(r)
        array.sort()
        return array

    def jogos(self):
        jogos = []
        while len(jogos) < self.__total:
            jogos.append(self._gera_dezenas())
        jogos.sort()
        self.__jogos.append(jogos)

    def sorteio(self):
        resultado = []
        while len(resultado) < 6:
            r = random.randint(1, 60)
            if r not in resultado:
                resultado.append(r)
        resultado.sort()
        self.__resultado.append(resultado)

    def tabela(self):
        jogos = ""
        qnt = 0
        for jogo in self.__jogos:
            for j in jogo:
                jogos = jogos + (f""" \
                    <tr> \
                        <td>{j}</td> \
                        <td>{len(j)}</td> \
                    </tr> \
                """)

        html = (f"""<table> \
            <tbody> \
                <tr> \
                    <td>Jogos</td> \
                    <td>Quantidade</td> \
                </tr> \
                {jogos} \
            </tbody> \
            </table>""")
        return html


mega_sena = Monetizze()
mega_sena.constroi(6, 2)
mega_sena.jogos()
mega_sena.sorteio()

jogos = mega_sena._Monetizze__jogos
resultado = mega_sena._Monetizze__resultado
html = mega_sena.tabela()

with open("resultados.html", "wb") as r:
    r.write(html.encode())

print("https://html-online.com/editor/")
print(html)